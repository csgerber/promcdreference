## McDonald's Reference App

This is the McDondald's reference implementation.

Speech options: both to search for places when adding a place and to search within actual contacts.
If speech search for adding a place fails, the phone will verbally prompt the user for how the query
should be said.

Sharing: can add someone directly from your contacts. Includes their phone, email, address if
available, and uses their picture from contacts if available.

UX: Hopefully it's easy to use...

Categories: There are two types of categories: I retained the favoriting, and added a category 
to classify. There are only three but it would easily scale to add more: Bars, Restaurants, 
and Other. Contacts default to other, yelp searches default to restaurants. 
The tab colors indicate favorite status, the background color indicates what type. 

This can be changed in...

Editing: You can edit Name, City, Address, Phone, Favorite Status, and classification for any 
entry.

Searching/Filtering: Can use the nav bar to filter type (All/Other/Restaurants/Bars), searching...
searches, and you can edit the way the list is sorted in settings.

Misc: You can also edit the default city for yelp searches in settings. Just because I felt like 
there should be more than one setting and couldn't think of anything better.

