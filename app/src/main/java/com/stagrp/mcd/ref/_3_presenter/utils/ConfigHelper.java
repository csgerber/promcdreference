package com.stagrp.mcd.ref._3_presenter.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


/**
 * The ConfigHelper assists in reading in and using any JSON configuation options for a particular
 * country. Provides static methods primarily used by App class for management and view classes
 * for any view details specific to a given country.
 */
public class ConfigHelper {

    private static final int SUBSTRING_START_POSITION = 4;
    private static final int BUFFER_SIZE = 8192;
    private static String TAG = "ConfigHelper";
    private Context mContext = null;
    private static Map<String, Object> mConfigMap = null;

    /**
     * Gets context.
     *
     * @return the context
     */
    public Context getContext() {
        return mContext;
    }

    /**
     * Sets context.
     *
     * @param mContext the context
     */
    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * Read json from file.
     *
     * @param context      the context
     * @param jsonFileName the json file name
     */
    public static void readJsonFromFile(Context context, String jsonFileName) {
        String jsonString = "";
        InputStream input;
        AssetManager assetManager = context.getAssets();
        try {
            input = assetManager.open(jsonFileName);
            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();
            jsonString = new String(buffer);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            jsonString = null;
        }

        mConfigMap = new Gson().fromJson(jsonString, HashMap.class);


    }


    /**
     * Get value  the appconfiguration currently loaded. The appconfiguration json controls which
     * DB is used (database), the "analytics" implementation, and whether the user sees a row
     * vs grid (style).
     *
     * @param minorKey "language", "locale", "database", "analytics", or "style"
     * @return  string -- the appconfiguration for the locale
     */
    public static String getValueGivenKey(String minorKey){
        if (mConfigMap == null){
            return "";
        }
        LinkedTreeMap<String,String> treeMap = (LinkedTreeMap<String, String>) mConfigMap.get("AppConfiguration");
        return  treeMap.get(minorKey);
    }




}
