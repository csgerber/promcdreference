package com.stagrp.mcd.ref._4_view.interfaces;

import com.stagrp.mcd.ref._1_data.core.yelpapi.YelpResultsData;


/**
 * The View for the {@link com.stagrp.mcd.ref._4_view.frags.NewFragment}.
 */
public interface NewView extends BaseView {

    /**
     * Display search results. Puts the YelpResultsData in a bundle and fires off to a
     * ResultsFragment.
     *
     * @param results the YelpResultsData from a search.
     */
    void displaySearchResults(YelpResultsData results);
}
