package com.stagrp.mcd.ref._4_view.frags;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stagrp.mcd.ref.R;
import com.stagrp.mcd.ref._1_data.core.yelpapi.YelpResultsData;
import com.stagrp.mcd.ref._3_presenter.ResultsPresenter;
import com.stagrp.mcd.ref._4_view.interfaces.ResultView;

import java.util.ArrayList;


/**
 * This fragment displays the yelp results data and saves the clicked restaurant to the database.
 * Bundle with the data is passed to the fragment, and when a place is clicked
 * the presenter fires off a CreatePlaceUseCase and the app returns to the Placefragment.
 */
public class ResultsFragment extends BaseFragment<ResultsPresenter> implements ResultView {

    private static final String RESULT_DATA = "results";
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private YelpResultsData resultsData;

    private interface OnItemSelectedListener {
        /**
         * Generic interface for action to be taken when a business is clicked
         *
         * @param position location in array
         */
        void itemSelected(int position);
    }


    /**
     * New instance results fragment. Has to parse a bundle containing {@link YelpResultsData}
     *
     * @param resultData the YelpResultsData
     * @return resultsFragment instance
     */
    public static ResultsFragment newInstance(YelpResultsData resultData) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(RESULT_DATA, resultData);
        ResultsFragment resultsFragment = new ResultsFragment();
        resultsFragment.setArguments(bundle);
        return resultsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_result, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.result_recycler);

        resultsData = (YelpResultsData) getArguments().getSerializable(RESULT_DATA);
        if (resultsData != null) {
            adapter = new RecyclerAdapter(resultsData, new OnItemSelectedListener() {
                @Override
                public void itemSelected(int position) {
                    presenter.saveClickedItem(resultsData.businesses.get(position));
                    //call back to the single Activity that hosts all frags
                    //when .showPlacesFragment() is shown, then onStart(), onResume() and onStop() will be called

                    getRouter().showPlacesFragment();

                }
            });
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

    @Override
    protected ResultsPresenter createPresenter() {
        return new ResultsPresenter();
    }

    // TODO: is this the best place for this? It may or may not be, but I'm not sure
    // Where to put it

    /**
     * Small RecyclerAdapter implementation for holding basic results data.
     */
    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

        private ArrayList<String> data;
        private OnItemSelectedListener onItemSelectedListener;


        /**
         * The ViewHolder for this RecyclerAdapter
         */
        public class ViewHolder extends RecyclerView.ViewHolder {
            /**
             * The TextView, only element in each row.
             */
            public TextView mTextView;

            /**
             * Instantiates a new View holder.
             *
             * @param v the viewHolder
             */
            public ViewHolder(View v) {
                super(v);
                mTextView = (TextView) v.findViewById(R.id.pop_text);
            }
        }

        /**
         * Instantiates a new Recycler adapter.
         *
         * @param resultsData            the yelpResultsData
         * @param onItemSelectedListener the onItemSelectedListener defined above
         */
        public RecyclerAdapter(YelpResultsData resultsData, OnItemSelectedListener onItemSelectedListener) {
            data = resultsData.getSimpleValues();
            this.onItemSelectedListener = onItemSelectedListener;
        }

        @Override
        public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pop_layout, parent, false);
            return new ViewHolder(v);

        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.mTextView.setText(data.get(position));
            holder.mTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemSelectedListener.itemSelected(position);
                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }
}
