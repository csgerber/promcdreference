package com.stagrp.mcd.ref._1_data.core;

import com.stagrp.mcd.ref.App;
import com.stagrp.mcd.ref._1_data.core.yelpapi.Yelp;
import com.stagrp.mcd.ref._1_data.core.yelpapi.YelpResultsData;

/**
 * Implementation of the {@link RestaurantDirectoryInterface}. See that page for description of
 * methods.
 */

public class YelpWrapper implements RestaurantDirectoryInterface {


    @Override
    public YelpResultsData fetchData(String name, String location) throws YelpSearchException {
        try {
            Yelp yelpApi = new Yelp();
            YelpResultsData yelpSearchResultLocal = null;
            yelpSearchResultLocal = yelpApi.searchMultiple(name, location);
            if(yelpSearchResultLocal.businesses == null) throw new YelpSearchException("No results" +
                    "found for search");
            App.setLastSearched(location);
            return yelpSearchResultLocal;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
