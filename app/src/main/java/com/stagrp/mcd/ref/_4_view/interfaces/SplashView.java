package com.stagrp.mcd.ref._4_view.interfaces;

/**
 * View for the {@link com.stagrp.mcd.ref._4_view.frags.SplashFragment}.
 * This is a simple fragment so no methods are defined here.
 */

public interface SplashView extends BaseView {
}
