package com.stagrp.mcd.ref._4_view.frags;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import com.stagrp.mcd.ref.R;
import com.stagrp.mcd.ref._3_presenter.SplashPresenter;
import com.stagrp.mcd.ref._4_view.interfaces.SplashView;

/**
 * Created by agerber on 9/12/2016.
 */
public class SplashFragment extends BaseFragment<SplashPresenter> implements SplashView {


    public static SplashFragment newInstance() {
        return new SplashFragment();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_splash, container, false);
        getActivity().overridePendingTransition(
                R.anim.slide_in_right,
                R.anim.slide_out_left
        );
        ImageView splashImg = (ImageView) v.findViewById(R.id.iv_splash);
        Glide.with(getContext())
                .load(R.drawable.splash_screen_n).centerCrop()
                .into(splashImg);
        // Load the sleeping async task and dummy data
        return v;
    }


    @Override
    protected SplashPresenter createPresenter() {
        return new SplashPresenter();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
