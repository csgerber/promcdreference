package com.stagrp.mcd.ref._3_presenter;

import java.util.Date;

import com.stagrp.mcd.ref._1_data.core.yelpapi.Business;
import com.stagrp.mcd.ref._2_interactor.CreatePlaceUseCase;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;
import com.stagrp.mcd.ref._4_view.interfaces.ResultView;


/**
 * The ResultsPresenter is the presenter for the fragment which displays {@link com.stagrp.mcd.ref._1_data.core.yelpapi.YelpResultsData}
 * The fragment receives a bundle containing the data to be displayed. The only logic
 * needed in this presenter is a function for saving a place clicked by the user. No
 * callback is necessary as the fragment handles navigation back to the Placefragment.
 */
public class ResultsPresenter extends Presenter<ResultView> {

    //The logic needs to be that any buttons and textviews can be registered in the view.
    // Any adapters, must be registered in the presenter.

    /**
     * Save clicked item. Fires off a createPlaceUseCase.
     *
     * @param business the business clicked to be saved as a {@link PlaceModel}
     */
    public void saveClickedItem(Business business) {

        PlaceModel placeModel = new PlaceModel(
                0,
                business.name,
                business.location.city,
                business.location.address.get(0),
                business.phone,
                business.url,
                business.image_url,
                business.categories.get(0).get(1),
                new Date().getTime(),
                PlacePresenter.sRESTAURANTS

        );

        CreatePlaceUseCase createPlaceUseCase = new CreatePlaceUseCase(null);
        createPlaceUseCase.execute(placeModel);

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }


}
