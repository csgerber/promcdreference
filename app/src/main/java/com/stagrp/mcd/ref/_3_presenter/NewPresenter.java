package com.stagrp.mcd.ref._3_presenter;

import com.stagrp.mcd.ref.App;
import com.stagrp.mcd.ref._1_data.core.yelpapi.YelpResultsData;
import com.stagrp.mcd.ref._2_interactor.FetchYelpResultsUseCase;
import com.stagrp.mcd.ref._4_view.interfaces.NewView;

/**
 * Implementation of the base Presenter for the fragment which searches results. This implements
 * the FetchYelpResultsUseCase to search in the a background thread.
 */
public class NewPresenter extends Presenter<NewView> implements
        FetchYelpResultsUseCase.YelpUseCaseCallback
{
    @Override
    public void onStart() {}

    @Override
    public void onResume() {}

    @Override
    public void onStop() {}

    private Boolean searchCanceled = false;

    /**
     * Cancel search.
     */
    public synchronized void cancelSearch(){ searchCanceled = true; }

    /**
     * Perform yelp search on specified name (restaurant name) and city/location
     *
     * @param name the name
     * @param city the city
     */
    public void performSearch(String name, String city) {

        view.startProgress();
        if(App.isNetworkConnected()) {
            FetchYelpResultsUseCase fetchYelpResultsUseCase = new FetchYelpResultsUseCase(this);
            fetchYelpResultsUseCase.execute(name, city);
        }
        else {
            if(city.equals(App.getLastSearched())) view.displaySearchResults(App.getLastResults());
            else view.showToast("No cached results for: " + city);
            view.stopProgress();
        }
    }

    @Override
    public void onDestroy() {

    }

    /**
     * Implementation of the YelpUseCaseCallBack
     * @param results here an instance of YelpResultsData returned by the search. may be null (empty)
     */
    @Override
    public void passPostExecute(YelpResultsData results) {

        // If our last search was canceled, ignore the results and clear canceled status

        synchronized (searchCanceled) {
            if (searchCanceled) {
                searchCanceled = false;
                return;
            }
        }
        // Fetchyelpresultscase returns null if nothing found
        if (results == null) {
            view.stopProgress();
            view.showToast("Search error! Try again");
            return;
        }
        view.stopProgress();
        App.getAnalytics().sendMessage("SEARCHING!");
        view.displaySearchResults(results);
        App.setLastResults(results);
    }

}
