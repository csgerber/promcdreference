package com.stagrp.mcd.ref._1_data.core;

import java.util.List;

import com.stagrp.mcd.ref._1_data.model.PlaceModel;


/**
 * The abstraction for a Repository pattern which the backend DB of choice (here Realm and SQLite
 * are implemented) <i>must</i> implement. This interface only houses basic CRUD operations and
 * should be primarily called from the interactor layer (UseCases).
 */
public interface RepoInterface {

    /**
     * Create.
     *
     * @param place Create an object in the database from {@link PlaceModel}
     */
    void create(PlaceModel place);

    /**
     * Get a {@link PlaceModel} out of the database based on the internal id.
     *
     * @param id internal id of the Place
     * @return PlaceModel
     */
    PlaceModel read(String id);

    /**
     * Update the db representation of the provided {@link PlaceModel}
     *
     * @param place PlaceModel
     */
    void update(PlaceModel place);

    /**
     * Delete the {@link PlaceModel} from of the database based on the internal id.
     *
     * @param id the id of the PlaceModel
     */
    void delete(String id);

    /**
     * Get the full list of {@link PlaceModel} from the database
     *
     * @return List of PlaceModels
     */
    List<PlaceModel> list();

 //   List<PlaceModel> filterByFavorite(String field, int val);

}
