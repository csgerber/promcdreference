package com.stagrp.mcd.ref._2_interactor;

import android.support.annotation.UiThread;

import java.util.List;

import com.stagrp.mcd.ref.App;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;

/**
 * DeletePlaceUseCase is an abstraction which interacts with the {@link com.stagrp.mcd.ref._1_data.core.RepoInterface}
 * repository to delete Places in the database based on a given {@link PlaceModel}.
 *
 * See {@link UseCase} for descriptions of UseCase design.
 */
public class DeletePlaceUseCase extends UseCase<PlaceModel, Void, List<PlaceModel>> {
    /**
     * Instantiates a new DeletePlaceUseCase. This takes a PlaceModel as input and will return
     * a list of PlaceModels for the view to update with if desired.
     *
     * @param caseCallback the caseCallback implemented by the presenter instantiating the usecase
     */
    public DeletePlaceUseCase(UseCaseCallback caseCallback) {
        super(caseCallback);
    }

    @UiThread
    @Override
    protected List<PlaceModel> doInBackground(PlaceModel... params) {
        App.getRepo().delete(params[0].getId());
        return App.getRepo().list();


    }

    /**
     * The interface DeletePlaceCaseCallback must be defined by the presenter instiating this
     * usecase. Use it to have the view update with a desired action on execution.
     */
    public interface DeletePlaceCallback extends UseCaseCallback<List<PlaceModel>>{}



    @Override
    protected void onPostExecute(List<PlaceModel> placeModels) {
        callback.passPostExecute( placeModels);
    }

}
