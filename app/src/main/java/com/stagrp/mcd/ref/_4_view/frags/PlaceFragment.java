package com.stagrp.mcd.ref._4_view.frags;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import com.stagrp.mcd.ref.R;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;
import com.stagrp.mcd.ref._3_presenter.PlacePresenter;
import com.stagrp.mcd.ref._3_presenter.adapters.PlaceAdapter;
import com.stagrp.mcd.ref._4_view.interfaces.PlaceView;


public class PlaceFragment extends BaseFragment<PlacePresenter> implements PlaceView {


    protected ListView listView;
    protected GridView gridView;

    protected ProgressBar progress_bar;
    protected TextView emptyTextView;

    protected PlaceAdapter mPlaceAdapter;


    public enum Type {
        LIST, GRID
    }


    public PlaceFragment() {

    }

    public static PlaceFragment newInstance(Type type) {

        PlaceFragment placeFragment = new PlaceFragment();
        Bundle bundle = new Bundle();
        bundle.putString("TYPE", type.toString());
        placeFragment.setArguments(bundle);
        return placeFragment;
    }

    @Override
    protected PlacePresenter createPresenter() {
        presenter = new PlacePresenter();
        return presenter;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view;


       if ( getArguments().getString("TYPE").equals(Type.LIST.toString())){
            view =  inflater.inflate(R.layout.fragment_place_list, container, false);
           listView = (ListView) view.findViewById(R.id.place_list_view);
        } else {
           view =  inflater.inflate(R.layout.fragment_place_grid, container, false);
           gridView = (GridView) view.findViewById(R.id.place_grid_view);

       }


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getRouter().showEditFragment(null);
            }
        });

        progress_bar = (ProgressBar)view.findViewById(R.id.progress_bar);
        progress_bar.setVisibility(View.INVISIBLE);
        emptyTextView = (TextView) view.findViewById(R.id.empty_textview);



        return view;
    }



    @Override
    public void startProgress() {
        super.startProgress();
        progress_bar.setVisibility(View.VISIBLE);

    }

    @Override
    public void stopProgress() {
        super.stopProgress();
        progress_bar.setVisibility(View.INVISIBLE);
    }

    //this method is defined in the PlaceView interface and is called from the presenter (PlacesPresenter)
    @Override
    public void displayPlaces(PlaceAdapter placeAdapter, List<PlaceModel> places) {

        if (mPlaceAdapter == null){
            mPlaceAdapter = placeAdapter;
            if ( getArguments().getString("TYPE").equals(Type.GRID.toString()))
              gridView.setAdapter(mPlaceAdapter);
            else
                listView.setAdapter(mPlaceAdapter);

        }
        mPlaceAdapter.setResults(places);
        mPlaceAdapter.notifyDataSetChanged();

        if (places == null || places.size() == 0){

            if ( getArguments().getString("TYPE").equals(Type.GRID.toString()))
                gridView.setEmptyView(emptyTextView);
            else
                listView.setEmptyView(emptyTextView);


        }

    }

    @Override
    public void onStop() {
        super.onStop();
        mPlaceAdapter = null;
    }
}
