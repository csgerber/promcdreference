package com.stagrp.mcd.ref._3_presenter.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * The NetworkServiceManager is used by the base App to provide a method available
 * for the presentation layer to check status. App essentially wraps isNetworkAvailable in a
 * static method.
 */
public class NetworkServiceManager extends ContextWrapper {

    /**
     * Instantiates a new Network service manager.
     *
     * @param base the Context, probably App
     */
    public NetworkServiceManager(Context base) {
        super(base);
    }

    /**
     * Return if the network is available
     *
     * @return boolean
     */
    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

}