package com.stagrp.mcd.ref._4_view.frags;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.stagrp.mcd.ref._3_presenter.Presenter;
import com.stagrp.mcd.ref._4_view.Router;
import com.stagrp.mcd.ref._4_view.interfaces.BaseView;


public abstract class BaseFragment<T extends Presenter> extends Fragment implements BaseView {

    protected T presenter;

    protected abstract T createPresenter();


    public Router getRouter() {
        return (Router) getActivity();
    }

    public final T getPresenter() {
        return presenter;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (presenter == null) {
            presenter = createPresenter();

        }
        presenter.setView(this);

    }


    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }


    @Override
    public void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    public void startProgress() {

    }

    @Override
    public void stopProgress() {

    }

    @Override
    public Context provideContext() {
        return this.getContext();
    }

    @Override
    public void showToast(String text) {
        Router navActivity = getRouter();
        if (navActivity == null) {
            return;
        }
        navActivity.showToast(text);
    }
}
