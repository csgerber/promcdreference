package com.stagrp.mcd.ref._1_data.core;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.stagrp.mcd.ref.App;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Wrapper for {@link RepoInterface} implementing SQLite. SQLite requires quite a bit more
 * setup than Realm, so this file is much longer than its Realm equivalent.
 *
 * <p>Variables in this file follow the pattern of COL_XXX to store the string name
 * of the database column, and INDEX_XXX to store the index number of the column.</p>
 * <p>
 * The CRUD methods specified by {@link RepoInterface} are described in that file.</p>
 *
 * Note that there is no equivalent to {@link RealmMapper} as SQLite is not an object database.
 * The equivalent actions (translating a {@link PlaceModel} to the db) are taken care of here.
 */
public class SQLiteWrapper implements RepoInterface {

    // Static strings for db creation, schema

    public static final String COL_ID = "id";
    public static final String COL_FAVORITE = "favorite";
    public static final String COL_NAME = "name";
    public static final String COL_CITY = "city";
    public static final String COL_ADDRESS = "address";
    public static final String COL_PHONE = "phone";
    public static final String COL_YELP = "yelp";
    public static final String COL_IMAGEURL = "imageURL";
    public static final String COL_CATEGORIES = "categories";
    public static final String COL_TIMESTAMP = "timestamp";

    /**
     * The constant COLUMN_LIST is simply convenient shorthand for referring to all of the COL_XXX
     * variables when making queries.
     */

    public static final String[] COLUMN_LIST = {COL_ID,
            COL_FAVORITE, COL_NAME, COL_CITY, COL_ADDRESS,
            COL_PHONE, COL_YELP, COL_IMAGEURL, COL_CATEGORIES, COL_TIMESTAMP};

    //these are the corresponding indices
    public static final int INDEX_ID = 0;
    public static final int INDEX_FAVORITE = INDEX_ID + 1;
    public static final int INDEX_NAME = INDEX_ID + 2;
    public static final int INDEX_CITY = INDEX_ID + 3;
    public static final int INDEX_ADDRESS = INDEX_ID + 4;
    public static final int INDEX_PHONE = INDEX_ID + 5;
    public static final int INDEX_YELP = INDEX_ID + 6;
    public static final int INDEX_IMAGEURL = INDEX_ID + 7;
    public static final int INDEX_CATEGORIES = INDEX_ID + 8;
    public static final int INDEX_TIMESTAMP = INDEX_ID + 9;


    /**
     * The constant TAG is used as the tag for logging any errors relating to SQLite.
     */
    private static final String TAG = "RestaurantDbWrapper";

    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    private static final String DATABASE_NAME = "dba_mcrestaurants";
    private static final String TABLE_NAME = "dba_mcrestaurants";
    private static final int DATABASE_VERSION = 2;
    private final Context mCtx = App.getContext();

    /**
     * The constant DATABASE_CREATE is just shorthand for creating all the COL_XXX rows.
     */

    private static final String DATABASE_CREATE =
            "CREATE TABLE if not exists " + TABLE_NAME + " ( " +
                    COL_ID + " INTEGER PRIMARY KEY autoincrement, " +
                    COL_FAVORITE + " INTEGER , " +
                    COL_TIMESTAMP + " LONG , " +
                    COL_NAME + " TEXT ," +
                    COL_CITY + " TEXT ," +
                    COL_ADDRESS + " TEXT ," +
                    COL_PHONE + " TEXT ," +
                    COL_YELP + " TEXT ," +
                    COL_IMAGEURL + " TEXT ," +
                    COL_CATEGORIES + " TEXT );";

    /**
     * Instantiates a new SQLite wrapper. {@link #open()} houses the instantiation logic.
     */
    public SQLiteWrapper() {}

    @Override
    public void create(PlaceModel place) {
        open();
        ContentValues values = putContentValues(place);
        mDb.insert(TABLE_NAME, null, values);
        close();
    }

    @Override
    public PlaceModel read(String id) {
        open();
        Cursor cursor = mDb.query(TABLE_NAME, COLUMN_LIST, COL_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null
        );
        if (cursor != null)
            cursor.moveToFirst();
        PlaceModel returnPlace = getPlaceFromCursor(cursor);
        close();
        return returnPlace;

    }

    @Override
    public void update(PlaceModel place) {
        open();
        ContentValues values = putContentValues(place);
        mDb.update(TABLE_NAME, values,
                COL_ID + "=?", new String[]{place.getId()});
        close();
    }

    @Override
    public void delete(String id) {
        open();
        mDb.delete(TABLE_NAME, COL_ID + "=?", new String[]{String.valueOf(id)});
        close();
    }

    @Override
    public List<PlaceModel> list() {
        open();
        Cursor cursor = mDb.query(TABLE_NAME, COLUMN_LIST, null, null, null, null, COL_TIMESTAMP+ " DESC");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        List<PlaceModel> list =  getPlaceListFromCursor(cursor);
        close();
        return list;
    }


    /**
     * Open the SQLite database. Instantiate a {@link DatabaseHelper} to assist in logging
     * and set {@link #mDb} to refer to the opened database.
     * @throws SQLException the sql exception
     */

    public void open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
    }

    /**
     * Close the SQLite database by closing the {@link DatabaseHelper} for this db.
     */
    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    /**
     * Helper method for inserting values of the provided {@link PlaceModel} into a
     * ContentValues object to be inserted into the db.
     * @param place a PlaceModel object.
     * @return a ContentValues object to be inserted.
     */
    private ContentValues putContentValues(PlaceModel place) {
        ContentValues values = new ContentValues();
        values.put(COL_NAME, place.getName());
        values.put(COL_FAVORITE, place.getFavorite());
        values.put(COL_TIMESTAMP, place.getTimestamp());
        values.put(COL_CITY, place.getCity());
        values.put(COL_ADDRESS, place.getAddress());
        values.put(COL_PHONE, place.getPhone());
        values.put(COL_YELP, place.getYelp());
        values.put(COL_IMAGEURL, place.getImageUrl());
        values.put(COL_CATEGORIES, place.getCategories());
        return values;
    }

    /**
     * Get a {@link PlaceModel} from the current location of the SQLite cursor.
     * @param cursor the DB cursor
     * @return the PlaceModel currently pointed to by the cursor
     */
    private PlaceModel getPlaceFromCursor(Cursor cursor) {
        PlaceModel returnPlace = new PlaceModel();
        returnPlace.setId(Integer.toString(cursor.getInt(INDEX_ID)));
        returnPlace.setFavorite(cursor.getInt(INDEX_FAVORITE));
        returnPlace.setTimestamp(cursor.getLong(INDEX_TIMESTAMP));
        returnPlace.setName(cursor.getString(INDEX_NAME));
        returnPlace.setCity(cursor.getString(INDEX_CITY));
        returnPlace.setAddress(cursor.getString(INDEX_ADDRESS));
        returnPlace.setPhone(cursor.getString(INDEX_PHONE));
        returnPlace.setYelp(cursor.getString(INDEX_YELP));
        returnPlace.setImageUrl(cursor.getString(INDEX_IMAGEURL));
        returnPlace.setCategories(cursor.getString(INDEX_CATEGORIES));
        return returnPlace;
    }

    /**
     * Helper method used by {@link #list()} to return the entire list of PlaceModels from the cursor.
     * @param cursor the DB cursor
     * @return List of PlaceModel
     */
    private List<PlaceModel> getPlaceListFromCursor(Cursor cursor) {
        List<PlaceModel> placeList = new ArrayList<>();
        while(!cursor.isAfterLast()) {
            placeList.add(getPlaceFromCursor(cursor));
            cursor.moveToNext();
        }
        return placeList;
    }

    /**
     * DatabaseHelper assists in creating an SQLiteDatabase object and logging transactions.
     */
    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.w(TAG, DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy old data");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }
}
