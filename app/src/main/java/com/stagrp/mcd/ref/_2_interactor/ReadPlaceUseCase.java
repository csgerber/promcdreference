package com.stagrp.mcd.ref._2_interactor;

import android.support.annotation.UiThread;

import com.stagrp.mcd.ref.App;
import com.stagrp.mcd.ref._1_data.core.RepoInterface;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;


/**
 * An implementation of {@link UseCase} which is responsible for abstracting the
 * {@link RepoInterface#read(String)} function in addition to providing threaded operation to the user.
 *
 * Takes a string as input which corresponds to the ID of the place and returns a List of {@link PlaceModel}.
 * Returns the PlaceModel read in the database.
 */

public class ReadPlaceUseCase extends UseCase<String, Void, PlaceModel> {
    public ReadPlaceUseCase(UseCaseCallback caseCallback) {
        super(caseCallback);
    }

    @UiThread
    @Override
    protected PlaceModel doInBackground(String... params) {
        return App.getRepo().read(params[0]);
    }

    public interface ReadPlaceCallback extends UseCaseCallback<PlaceModel>{}

    @Override
    protected void onPostExecute(PlaceModel placeModel) {
        callback.passPostExecute(placeModel);
    }


}
