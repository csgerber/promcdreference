package com.stagrp.mcd.ref._3_presenter;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import com.stagrp.mcd.ref.App;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;
import com.stagrp.mcd.ref._3_presenter.utils.PrefsMgr;
import com.stagrp.mcd.ref._4_view.interfaces.SplashView;

import static java.util.UUID.randomUUID;

/**
 * Created by billobob on 9/21/16.
 */

public class SplashPresenter extends Presenter<SplashView> {


    private int SIMUATE_LOADING_TIME = 1500;

    @Override
    public void onStart() {



        splashLoad();
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onStop() {
    }

    @Override
    public void onDestroy() {

    }

    public void splashLoad() {
        // Background code to load data on splash screen
        new AsyncTask<Void, Void, Void>() {

            @Override
            public Void doInBackground(Void... params) {
                // Initialize PrefsMgr if necessary
                // I thought it makes sense to put this here because if necessary loading
                // the dummy data can be "hidden" by the load time
                if (PrefsMgr.getString(App.getContext(), PrefsMgr.CITY) == null)
                    PrefsMgr.setString(App.getContext(), PrefsMgr.CITY, "Chicago");
                if (PrefsMgr.getString(App.getContext(), PrefsMgr.SORT) == null)
                    PrefsMgr.setString(App.getContext(), PrefsMgr.SORT, "Time Entered");
                if (PrefsMgr.getString(App.getContext(), PrefsMgr.TYPE) == null)

                    // First run-initialization
                    // Note: first launch ONLY. If you use code to switch the repo it will appear
                    // blank (old db entries will persist of course)

                    if (PrefsMgr.getBoolean(App.getContext(), PrefsMgr.FIRST_RUN, true)) {
                        PrefsMgr.setBoolean(App.getContext(), PrefsMgr.FIRST_RUN, false);

                        List<PlaceModel> dummyPlaces = new ArrayList<>();

                        PlaceModel tempPlace = new PlaceModel();

                        tempPlace.setFavorite(0);
                        tempPlace.setType("bars");
                        tempPlace.setImageUrl("https://s3-media4.fl.yelpcdn.com/bphoto/qVfzTJYNjdGrMQRpWHJYJQ/o.jpg");
                        tempPlace.setAddress("1951 N Western Ave");
                        tempPlace.setCity("Chicago");
                        tempPlace.setPhone("555-555-5555");
                        tempPlace.setCategories("dummy");
                        tempPlace.setName("McDonald's");
                        tempPlace.setYelp("http://www.yelp.com");
                        tempPlace.setTimestamp(0);
                        tempPlace.setId(String.valueOf(randomUUID()));
                        dummyPlaces.add(tempPlace);

                        tempPlace = new PlaceModel();
                        tempPlace.setFavorite(0);
                        tempPlace.setType("bars");
                        tempPlace.setImageUrl("https://s3-media4.fl.yelpcdn.com/bphoto/UZVwLvOdyI2I3wEPob51Ag/o.jpg");
                        tempPlace.setAddress("600 N Clark St");
                        tempPlace.setCity("Chicago");
                        tempPlace.setPhone("555-555-5555");
                        tempPlace.setCategories("dummy");
                        tempPlace.setName("McDonald's");
                        tempPlace.setYelp("http://www.yelp.com");
                        tempPlace.setId(String.valueOf(randomUUID()));
                        tempPlace.setTimestamp(1);
                        dummyPlaces.add(tempPlace);

                        tempPlace = new PlaceModel();
                        tempPlace.setFavorite(0);
                        tempPlace.setType("bars");
                        tempPlace.setImageUrl("https://s3-media4.fl.yelpcdn.com/bphoto/faxg3V7gL82uRLAauw9fjg/o.jpg");
                        tempPlace.setAddress("0123 Somewhere Ave");
                        tempPlace.setCity("Chicago");
                        tempPlace.setPhone("555-555-5555");
                        tempPlace.setCategories("dummy");
                        tempPlace.setYelp("http://www.yelp.com");
                        tempPlace.setName("McDonald's");
                        tempPlace.setId(String.valueOf(randomUUID()));
                        tempPlace.setTimestamp(2);
                        dummyPlaces.add(tempPlace);

                        tempPlace = new PlaceModel();
                        tempPlace.setFavorite(0);
                        tempPlace.setType("bars");
                        tempPlace.setImageUrl("https://s3-media1.fl.yelpcdn.com/bphoto/ChpoEknkgHSN0Lu14ix7yg/o.jpg");
                        tempPlace.setAddress("456 Placeholder Street");
                        tempPlace.setCity("Chicago");
                        tempPlace.setPhone("555-555-5555");
                        tempPlace.setCategories("dummy");
                        tempPlace.setName("McDonald's");
                        tempPlace.setYelp("http://www.yelp.com");
                        tempPlace.setId(String.valueOf(randomUUID()));
                        tempPlace.setTimestamp(3);
                        dummyPlaces.add(tempPlace);

                        tempPlace = new PlaceModel();
                        tempPlace.setFavorite(0);
                        tempPlace.setType("bars");
                        tempPlace.setImageUrl("https://s3-media4.fl.yelpcdn.com/bphoto/C5-8jPooSLR07QfWyWSg8w/o.jpg");
                        tempPlace.setAddress("123 My City");
                        tempPlace.setCity("Chicago");
                        tempPlace.setPhone("555-555-5555");
                        tempPlace.setCategories("dummy");
                        tempPlace.setName("McDonald's");
                        tempPlace.setYelp("http://www.yelp.com");
                        tempPlace.setId(String.valueOf(randomUUID()));
                        tempPlace.setTimestamp(4);
                        dummyPlaces.add(tempPlace);

                        tempPlace = new PlaceModel();
                        tempPlace.setFavorite(0);
                        tempPlace.setType("bars");
                        tempPlace.setImageUrl("https://s3-media2.fl.yelpcdn.com/bphoto/RLMTSWFVRYfOAmBNvwEPVw/o.jpg");
                        tempPlace.setAddress("5454 N Western Ave");
                        tempPlace.setCity("Evanston");
                        tempPlace.setPhone("555-555-5555");
                        tempPlace.setCategories("dummy");
                        tempPlace.setYelp("http://www.yelp.com");
                        tempPlace.setName("McDonald's");
                        tempPlace.setId(String.valueOf(randomUUID()));
                        tempPlace.setTimestamp(5);
                        dummyPlaces.add(tempPlace);

                        // Not using a use-case here because it's hacky to begin with,
                        // And intent doesn't switch until the asynctask finishes anyway
                        for (PlaceModel place : dummyPlaces) App.getRepo().create(place);

                    }

                try {
                    Thread.sleep(SIMUATE_LOADING_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // Swap out for the place fragment
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                view.getRouter().showPlacesFragment();

            }
        }.execute(null, null, null);
    }
}
