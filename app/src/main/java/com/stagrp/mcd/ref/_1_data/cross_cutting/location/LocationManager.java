package com.stagrp.mcd.ref._1_data.cross_cutting.location;

import android.content.Context;
import android.telephony.TelephonyManager;

import java.util.Locale;

import com.stagrp.mcd.ref.App;

/**
 * Default implementation of the {@link LocationInterface}.
 */
public class LocationManager implements LocationInterface {


    /**
     * Get ISO 3166-1 alpha-2 country code for this device (or null if not available)
     * @return country code or null
     */
    @Override
    public String getCountryCode() {

        try {
            final TelephonyManager tm = (TelephonyManager) App.getContext().getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            }
            else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        }
        catch (Exception e) { }
        return null;
    }



}
