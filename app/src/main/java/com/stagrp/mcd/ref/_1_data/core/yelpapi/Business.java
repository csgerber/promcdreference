package com.stagrp.mcd.ref._1_data.core.yelpapi;

import java.io.Serializable;
import java.util.List;


/**
 * This is a simple POJO representation of the raw data returned from Yelp before it is
 * convereted to a {@link com.stagrp.mcd.ref._1_data.model.PlaceModel}. Note that
 * it contains a {@link Location} object which itself contains a {@link Coordinate} object
 * due to the nature of the data returned by yelp.
 */
public class Business implements Serializable {


        public String name = "";
        /**
         * The Url for the yelp site.
         */
        public String url = "";
        /**
         * URL for yelp thumbnail.
         */
        public String image_url = "";
        /**
         * Phone number.
         */
        public String phone = "";
        /**
         * A {@link Location} object, not yet a string.
         */
        public Location location;
        /**
         * The Categories; no POJO is created for this as we only pull one type of category
         * (always "restaurants" for McDonald's)
         */
        public List<List<String>> categories;
        /**
         * The Rating as a text string.
         */
        public String rating_img_url = "";

}
