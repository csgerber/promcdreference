package com.stagrp.mcd.ref._1_data.cross_cutting.location;

/**
 * An interface for getting the country code based on a user's current location. Only implemented
 * in {@link LocationManager}, but in theory a different implementation could be selected at
 * runtime based on specifications of a specific phone if necessary.
 */
public interface LocationInterface {

    /**
     * Get the country code based on current location.
     *
     * @return Simple string containing two-letter country code (e.g. "US").
     */
    String getCountryCode();
}
