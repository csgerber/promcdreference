package com.stagrp.mcd.ref._1_data.core.yelpapi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * The YelpResultsData class is the result of searches performed in the {@link com.stagrp.mcd.ref._4_view.frags.NewFragment}.
 * These results are passed on to a {@link com.stagrp.mcd.ref._4_view.frags.ResultsFragment}
 * and contain a list of the {@link Business} representation of yelp data (it is only converted
 * into a {@link com.stagrp.mcd.ref._1_data.model.PlaceModel} after the restaurant
 * has been clicked to save to the database).
 */
public class YelpResultsData implements Serializable {


    /**
     * List of Business objects (basic representation of Yelp data).
     */
    public List<Business> businesses;


    /**
     * Returns a basic string of information for the {@link com.stagrp.mcd.ref._4_view.frags.ResultsFragment}
     * to display. Contains the name, location, and star rating represented as text.
     *
     * @return the simple values
     */
    public ArrayList<String> getSimpleValues() {
        ArrayList<String> simpleValues = new ArrayList<>();

        try {
            for (Business biz : businesses) {

                simpleValues.add(biz.name + " | " + biz.location.address.get(0) + " | " + getStars(biz.rating_img_url));
            }
        } catch (Exception e) {
            //will continue on its own
            return null;
        }
        return simpleValues;
    }

    /**
     * Get the Yelp star rating and convert to text.
     * @param strUrl string containing the star rating as a text description
     * @return string of stars representing rating
     */
    private String getStars(String strUrl) {

        int nHalfCode = Integer.parseInt("00BD", 16);
        char[] cChars = Character.toChars(nHalfCode);
        String strHalf = String.valueOf(cChars);


        //try the most specific conditions first
        if (strUrl.contains("stars_4_half")) {
            return "****" + strHalf;
        } else if (strUrl.contains("stars_3_half")) {
            return "***" + strHalf;
        } else if (strUrl.contains("stars_2_half")) {
            return "**" + strHalf;
        } else if (strUrl.contains("stars_1_half")) {
            return "*" + strHalf;
        } else if (strUrl.contains("stars_5")) {
            return "*****";
        } else if (strUrl.contains("stars_4")) {
            return "****";
        } else if (strUrl.contains("stars_3")) {
            return "***";
        } else if (strUrl.contains("stars_2")) {
            return "**";
        } else if (strUrl.contains("stars_1")) {
            return "*";
        } else {
            return "";
        }
    }


}
