package com.stagrp.mcd.ref._1_data.cross_cutting.analytics;


import android.content.Context;
import android.util.Log;

import com.stagrp.mcd.ref.App;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Interface for communicating analytics messages. Will be implemented as a custom representation
 * depending on the country in which the user is located (no real analytics is reported in this
 * app, it is only intended as a demo of switching analytics in the same app based on location).
 * <p>
 * This sets up a ScheduledThreadPoolExecutor (which really just consists of one background thread)
 * to periodically write out the cache to the log file).
 */
public abstract class AnalyticsInterface {

    private final long LOG_DELAY_MINUTES = 1;

    private ScheduledExecutorService mScheduledExecutorService = null;
    /**
     * Write analytics data to a location set by each implementation.
     *
     * @param message A simple string containing a dummy analytics message
     */
    public void sendMessage(String... message) {
        // Just concatenates all the args into one big string
        StringBuilder fullMsg = new StringBuilder();
        for(String str: message) {
            fullMsg.append(str);
        }
        AnalyticsCache.getAnalyticsCache().addAnalyticsEvent(fullMsg.toString());
    }

    AnalyticsInterface() {
    }

    /**
     * Function to initialize the executor, whether for the first time in lifespan of the app
     * or after a shutdown. Every specified amount of time (LOG_DELAY_MINUTES) the in-memory
     * cached analytics logs will be fllushed to an output file.
     */
    public void initExecutor() {
            mScheduledExecutorService =
                    Executors.newSingleThreadScheduledExecutor();

            mScheduledExecutorService.scheduleAtFixedRate
                    (new Runnable() {
                        public void run() {
                            writeCacheToLog();
                        }
                    }, LOG_DELAY_MINUTES, LOG_DELAY_MINUTES, TimeUnit.MINUTES);
    }

    /**
     * Function to write the anayltics cache log to the output file specified by the country.
     */
    private void writeCacheToLog() {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                    App.getContext().openFileOutput(getLogFileName(), Context.MODE_PRIVATE));
            for(String logItem: AnalyticsCache.getAnalyticsCache().getAnalyticsLog()) {
                outputStreamWriter.write(logItem);
                Log.i("Analytics", logItem);
            }
            outputStreamWriter.close();
            AnalyticsCache.getAnalyticsCache().resetAnalyticsLog();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    abstract String getLogFileName();

    /**
     * Shutdown the executorservice and make a final write out to the cache
     */
    public void shutdown() {
        writeCacheToLog();
        mScheduledExecutorService.shutdown();
    }
}
