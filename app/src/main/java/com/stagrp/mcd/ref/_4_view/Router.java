package com.stagrp.mcd.ref._4_view;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.stagrp.mcd.ref.App;
import com.stagrp.mcd.ref.R;
import com.stagrp.mcd.ref._1_data.core.yelpapi.YelpResultsData;
import com.stagrp.mcd.ref._3_presenter.utils.ConfigHelper;
import com.stagrp.mcd.ref._3_presenter.utils.PrefsMgr;
import com.stagrp.mcd.ref._4_view.frags.BaseFragment;
import com.stagrp.mcd.ref._4_view.frags.NewFragment;
import com.stagrp.mcd.ref._4_view.frags.PlaceFragment;
import com.stagrp.mcd.ref._4_view.frags.ResultsFragment;
import com.stagrp.mcd.ref._4_view.frags.SplashFragment;

import java.util.Locale;


/**
 * The Router is our base Activity for the app, and is essentially a "super view". Note
 * that it has no presenter as the Router itself does not need to interact with any data
 * or UseCases, only manage physical navigation of the application.
 */
public class Router extends
        AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private Locale locale = null;


    private static final int USA_MENU_IDX = 1;
    private static final int CHINA_MENU_IDX = 2;

    /**
     * Convenience enum shorthand for clearing and pushing the backstack.
     */
    public enum Backstack {
        /**
         * Clear and push backstack.
         */
        CLEAR_AND_PUSH, /**
         * Push backstack.
         */
        PUSH
    }

    private ProgressBar progressBar;

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    /**
     * OnCreate here just does the standard anrdoid UI stuff: inflate the necessary UI options,
     * display the {@link SplashFragment} if it is the first launch of the application.
     * @param savedInstanceState savedInstanceState bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Resources res = getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(App.getMockCountry().toLowerCase());
        res.updateConfiguration(conf, dm);


        setContentView(R.layout.activity_nav);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.addView(new ImageView(this));
        setSupportActionBar(toolbar);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
      //  getSupportActionBar().setIcon(R.drawable.loading_icon_6);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        checkCountry(navigationView);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null){
            showSplashFragment();
        } else {
            showPlacesFragment();
        }



    }

    /**
     * Internal function for setting the UI based on current (mocked) locale.
     * @param navigationView the navigationView
     */
    private void checkCountry(NavigationView navigationView) {
        if(PrefsMgr.getString(getApplicationContext(), App.KEY_COUNTRY_CODE, "US").equals("US"))
            navigationView.getMenu().getItem(USA_MENU_IDX).setIcon(R.drawable.gear_dark);
        else navigationView.getMenu().getItem(CHINA_MENU_IDX).setIcon(R.drawable.gear_dark);
    }

    /**
     * Method for other views/fragments to show a toast.
     *
     * @param text the toast text
     */
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    /**
     * Navigate to the edit fragment.
     *
     * @param string string indicating what is being edited
     */
    public void showEditFragment(String string) {

        BaseFragment baseFragment = NewFragment.newInstance(string);
        pushFragmentOnStack(baseFragment, Backstack.PUSH);

    }

    /**
     * Navigate to the fragment.
     *
     * @param resultData the {@link YelpResultsData} passed to ResultsFragment.
     */
    public void showResultFragment(YelpResultsData resultData) {

        BaseFragment baseFragment = ResultsFragment.newInstance(resultData);
        pushFragmentOnStack(baseFragment, Backstack.PUSH);
    }


    /**
     * Navigate to the places fragment.
     */
    public void showPlacesFragment() {


        BaseFragment baseFragment = PlaceFragment.newInstance(
                ConfigHelper.getValueGivenKey("style").equals("row") ?
                PlaceFragment.Type.LIST : PlaceFragment.Type.GRID
              );

        pushFragmentOnStack(baseFragment, Backstack.CLEAR_AND_PUSH);

    }

    /**
     * Navigate to the splash fragment.
     */
    public void showSplashFragment(){



       BaseFragment baseFragment = SplashFragment.newInstance();
       pushFragmentOnStack(baseFragment, Backstack.CLEAR_AND_PUSH);

        ActionBar actionBar =   getSupportActionBar();
        if (actionBar != null)
            actionBar.hide();

    }


    /**
     * Convenience method for pushing a fragment to the backstack. We want to make
     * sure the ResultsFragment is always on the backstack after the SplashFragment has finished.
     *
     * @param baseFragment fragment to be placed
     * @param popBackStack the backstack enum defined above
     */
    private void pushFragmentOnStack(BaseFragment baseFragment, Backstack popBackStack) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (popBackStack == Backstack.CLEAR_AND_PUSH)
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment_container,baseFragment ,baseFragment.getClass().getName());
        ft.addToBackStack(ResultsFragment.class.getName());
        ft.commit();
        getSupportActionBar().show();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    /**
     * Navigation drawer. Used for toggling favorites (checkbox) and switching between "US" and
     * "China", which restarts the application to load the new country settings.
     *
     * @param item the menuItem selected
     * @return always true in the demo app
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        String strKeyCountry;

        switch (item.getItemId()){
            case R.id.nav_bars:
                android.support.v4.app.Fragment fragment = this.getSupportFragmentManager().findFragmentByTag(PlaceFragment.class.getName());
                if (fragment != null){
                    PlaceFragment placeFragment =(PlaceFragment) fragment ;
                    placeFragment.getPresenter().listFavs();
                }
                if(item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                break;

            case R.id.swap_china:

                 strKeyCountry = ConfigHelper.getValueGivenKey("locale");
                if (strKeyCountry.equals("US")){
                    PrefsMgr.setString(getApplicationContext(), App.KEY_COUNTRY_CODE, "CN");
                    restartOnMajorChange();
                }

                break;
            case R.id.swap_usa:

                 strKeyCountry = ConfigHelper.getValueGivenKey("locale");
                if (strKeyCountry.equals("CN")){
                    PrefsMgr.setString(getApplicationContext(), App.KEY_COUNTRY_CODE, "US");
                    restartOnMajorChange();
                }

                break;
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Convenience method to set an alarm for restarting the application on a "major change"
     * (here only changing the country).
     */
    private void restartOnMajorChange() {
        Intent mStartActivity = new Intent(getApplicationContext(), Router.class);
        int mPendingIntentId = 100001;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId,    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 10, mPendingIntent);
        System.exit(0);
    }


}
