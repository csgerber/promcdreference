package com.stagrp.mcd.ref._3_presenter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.stagrp.mcd.ref.R;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;

/**
 * The implementation of the {@link GenAdapter} for {@link PlaceModel}. This class houses
 * the logic which translates PlaceModels into elements of RecyclerView holding them.
 *
 * This is part of the view but housed separately as it is not its own fragment.
 */
public final class PlaceAdapter extends GenAdapter<PlaceModel, PlaceAdapter.ViewHolder> {

    private Context mContext;


    /**
     * The ViewHolder representing each individual place. Holds values from {@link PlaceModel}
     * which will be displayed in the RecyclerView.
     */
    public static class ViewHolder extends GenAdapter.ViewHolder {
        /**
         * The name of the place. In this app always McDonalds restaurants.
         */
        public TextView listText;
        /**
         * The City or ZIP of the place.
         */
        public TextView listCity;
        /**
         * The address of the restaurant.
         */
        public TextView listAddress;
        /**
         * The ImageView where the imageURL of the Place will be used.
         */
        public ImageView listNiv;
        /**
         * The listTab shows the status of a favorite (yellow = favorited).
         */
        public View listTab;

        public LinearLayout listRow;


        /**
         * Instantiates a new ViewHolder for an individual place. Set all the view
         * resources to their corresponding fields in {@link PlaceModel}
         *
         * @param container the container for the layout housing the ViewHolder
         */
        public ViewHolder(LinearLayout container) {
            super(container);
            this.listText = (TextView) container.findViewById(R.id.list_text);
            this.listCity = (TextView) container.findViewById(R.id.list_city);
            this.listNiv = (ImageView) container.findViewById(R.id.list_niv);
            this.listAddress = (TextView) container.findViewById(R.id.list_address);
            this.listTab = container.findViewById(R.id.list_tab);
            this.listRow = (LinearLayout) container.findViewById(R.id.place_entry);
        }
    }

    /**
     * The interface for an OnClickListener which will bring up a menu of options.
     */
    public interface OnItemClickListener {
        /**
         * On item click.
         *
         * @param example the example
         */
        void onItemClick(PlaceModel example);
    }

    private final LayoutInflater inflater;
    private final OnItemClickListener listener;

    /**
     * Instantiates a new Place adapter. This allows each row to handle a click for
     * its corresponding Place.
     *
     * @param context context reference for the adapter
     * @param listener the OnItemClickListener defined above
     */
    public PlaceAdapter(Context context, OnItemClickListener listener) {
        this.inflater = LayoutInflater.from(context);
        this.mContext = context;
        this.listener = listener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (parent.getId() == R.id.place_grid_view)
             v = inflater.inflate(R.layout.places_row_grid, parent, false);
        else
             v = inflater.inflate(R.layout.places_row_list, parent, false);

        return  new ViewHolder((LinearLayout) v);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        final PlaceModel item = getItem(position);

        holder.listText.setText(item.getName());
        holder.listCity.setText("City: " + item.getCity());
        holder.listAddress.setText("Address: " + item.getAddress());
        Glide.with(mContext)
                .load(item.getImageUrl())
                .placeholder(R.drawable.gear_dark)
                .into(holder.listNiv);

        holder.listTab.setBackgroundColor(item.getFavorite() == 0 ?
                holder.listTab.getResources().getColor(R.color.color_restaurant) : mContext.getResources().getColor(R.color.colorPrimary));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(item);
            }
        });

    }

}