package com.stagrp.mcd.ref._4_view.interfaces;

import android.content.Context;

import com.stagrp.mcd.ref._4_view.Router;


/**
 * A generic View to be implemented by all views. Contains a few functions which all views
 * should have access to (even if they do not use them).
 */
public interface BaseView {

    /**
     * Gets router (used for navigation and global notifications).
     *
     * @return the router
     */
    Router getRouter();

    /**
     * Provide the context for the view.
     *
     * @return the context
     */
    Context provideContext();

    /**
     * Start progress. Used for starting/stopping search operations.
     */
    void startProgress();

    /**
     * Stop progress. Used for starting/stopping search operations.
     */
    void stopProgress();

    /**
     * Show toast.
     *
     * @param message the message
     */
    void showToast(String message);
}
