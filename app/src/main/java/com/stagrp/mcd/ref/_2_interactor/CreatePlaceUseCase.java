package com.stagrp.mcd.ref._2_interactor;

import android.support.annotation.UiThread;

import com.stagrp.mcd.ref.App;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;

/**
 * CreatePlaceUseCase is an abstraction which interacts with the {@link com.stagrp.mcd.ref._1_data.core.RepoInterface}
 * repository to instantiate Places in the database based on a given {@link PlaceModel}.
 *
 * This method should only be called in the presenter layer and never used in the business logic
 * layers.
 *
 * See {@link UseCase} for descriptions of UseCase design.
 */

public class CreatePlaceUseCase extends UseCase<PlaceModel, Void, Object> {
    /**
     * Instantiates a new CreatePlaceUseCase. This takes a PlaceModel as input.
     *
     * @param caseCallback the caseCallback implemented by the presenter instantiating the usecase
     */

    public CreatePlaceUseCase(UseCaseCallback caseCallback) {
        super(caseCallback);
    }

    @UiThread
    @Override
    protected Object doInBackground(PlaceModel... params) {

        App.getRepo().create(params[0]);
        return new Object();

    }

    /**
     * This callback must be defined by any presenter creating a CreatePlaceUseCase. Used to have
     * the view perform a particular action upon execution of the thread.
     */
    public interface CreatePlaceCallback extends UseCaseCallback<Object>{}



    @Override
    protected void onPostExecute(Object object) {
        if (callback != null)
           callback.passPostExecute( object);
    }
}
