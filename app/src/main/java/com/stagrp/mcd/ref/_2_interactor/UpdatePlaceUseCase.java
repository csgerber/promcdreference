package com.stagrp.mcd.ref._2_interactor;

import android.support.annotation.UiThread;

import java.util.List;

import com.stagrp.mcd.ref.App;
import com.stagrp.mcd.ref._1_data.core.RepoInterface;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;

/**
 * An implementation of {@link UseCase} which is responsible for abstracting the
 * {@link RepoInterface#update(PlaceModel)} function in addition to providing threaded operation to the user.
 *
 * Takes a PlaceModel to be modified as input and returns the List of PlaceModels in the repository.
 */
public class UpdatePlaceUseCase extends UseCase<PlaceModel, Void, List<PlaceModel>> {
    public UpdatePlaceUseCase(UseCaseCallback caseCallback) {
        super(caseCallback);
    }

    @UiThread
    @Override
    protected List<PlaceModel> doInBackground(PlaceModel... params) {

        App.getRepo().update(params[0]);
        return App.getRepo().list();


    }

    public interface UpdatePlaceCallback extends UseCaseCallback<List<PlaceModel>>{}

    @Override
    protected void onPostExecute(List<PlaceModel> placeModels) {
        callback.passPostExecute(placeModels);
    }


}
