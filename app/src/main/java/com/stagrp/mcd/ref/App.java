package com.stagrp.mcd.ref;

import android.app.Application;
import android.content.Context;

import com.stagrp.mcd.ref._1_data.core.RealmWrapper;
import com.stagrp.mcd.ref._1_data.core.RepoInterface;
import com.stagrp.mcd.ref._1_data.core.RestaurantDirectoryInterface;
import com.stagrp.mcd.ref._1_data.core.SQLiteWrapper;
import com.stagrp.mcd.ref._1_data.core.YelpWrapper;
import com.stagrp.mcd.ref._1_data.core.yelpapi.YelpResultsData;
import com.stagrp.mcd.ref._1_data.cross_cutting.analytics.AnalyticsInterface;
import com.stagrp.mcd.ref._1_data.cross_cutting.analytics.GaWrapper;
import com.stagrp.mcd.ref._1_data.cross_cutting.analytics.InsightsWrapper;
import com.stagrp.mcd.ref._1_data.cross_cutting.location.LocationInterface;
import com.stagrp.mcd.ref._1_data.cross_cutting.location.LocationManager;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;
import com.stagrp.mcd.ref._3_presenter.utils.ConfigHelper;
import com.stagrp.mcd.ref._3_presenter.utils.NetworkServiceManager;
import com.stagrp.mcd.ref._3_presenter.utils.PrefsMgr;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static com.stagrp.mcd.ref._3_presenter.utils.PrefsMgr.VERSION;
import static com.stagrp.mcd.ref._3_presenter.utils.PrefsMgr.VERSION_KEY;

/**
 * The App class is the overall "manager" of the application--holding references to most singletons
 * (e.g. the RepoInterface) and sitting above the MVPI layers. It also assists with holding
 * in-memory cache of places, as well as the last place searched for purposes of offline caching.
 *
 * Note that this contains no imports from the "deeper" layers of Android, only requiring Application
 * and Context. This should in theory lend to easier testing, as the App class is mostly only
 * dependent on imports from other layers of the application.
 */
public class App extends Application {


    public static Context sContext;

    private static RepoInterface sCrudList;
    private static RestaurantDirectoryInterface sYelpInterface;
    private static AnalyticsInterface sAnalyticsInterface;
    private static LocationInterface sLocationInterface;

    // Cache for presentation only (never modified)
    private static List<PlaceModel> sPlaceModels;

    // Cache for last results. Currently doesn't persist on app close
    private static YelpResultsData sLastResults = null;
    private static String sLastSearched = null;
    private static boolean sNetworkConnected;

    /**
     * The constant KEY_COUNTRY_CODE. Used for mocking the locale setting as we don't have a native
     * Chinese phone to test on.
     */
    public static String KEY_COUNTRY_CODE = "ISO_3166-1_ALPHA-2_COUNTRY_CODE";

    private static String sMockCountry;

    /**
     * Instantiates a new App, empty constructor.
     */
    public App() {
    }

    /**
     * Gets context.
     *
     * @return the App context
     */
    public static Context getContext() {
        return sContext;
    }


    /**
     * In the App onCreate we want to make sure the locale is set so the appropriate json
     * file is read from for purposes of choosing the database and UI. It also checks for
     * connectivity to set the initial connectivity state of the application.
     *
     * Locale defaults to US.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();

       String strCountry = PrefsMgr.getString(getApplicationContext(), App.KEY_COUNTRY_CODE, "US");
        switch (strCountry){
            case "US":
                ConfigHelper.readJsonFromFile(this, "gma_build_config_en_us.json");
                break;

            case "CN":
                ConfigHelper.readJsonFromFile(this, "gma_build_config_en_cn.json");
                break;

            default:
                ConfigHelper.readJsonFromFile(this, "gma_build_config_en_us.json");
                break;
        }





        // First instantiation of the app, load the splash
        PrefsMgr.setBoolean(this, PrefsMgr.SPLASH_LOADED, false);

        // If using realm and it needs to be reset
        int storedVersion = PrefsMgr.getInt(this, PrefsMgr.VERSION_KEY, 0);
            if (storedVersion == 0 || storedVersion != VERSION) {
                resetRealm();
                PrefsMgr.setInt(this, VERSION_KEY, VERSION);
        }

        sMockCountry = ConfigHelper.getValueGivenKey("locale");
        boolean networkStatus = new NetworkServiceManager(this).isNetworkAvailable();
        setNetworkConnected(networkStatus);


    }


    /**
     * Gets the RepoInterface used in the interactor layer for CRUD operations. Defaults
     * to SQLite.
     *
     * @return the RepoInterface
     */
    public static RepoInterface getRepo(){
        if (sCrudList == null){

            switch (ConfigHelper.getValueGivenKey("database")){
                case "SQLite":
                    sCrudList = new SQLiteWrapper();
                    break;

                case "Realm":
                    sCrudList = new RealmWrapper();
                    break;

                default:
                    sCrudList = new SQLiteWrapper();
                    break;
            }
        }
        return sCrudList;
    }


    /**
     * Get RestaurantDirectoryInterface; in this application only Yelp is implemented, but
     * in theory it would not be too hard to develop another interface for different search APIs
     * (e.g. TripAdvisor).
     *
     * @return the RestaurantDirectoryInterface
     */
//repositiory
    public static RestaurantDirectoryInterface getRestaurnatDirectory(){
        if (sYelpInterface == null){
            sYelpInterface = new YelpWrapper();
        }
        return sYelpInterface;
    }


    // TODO: make messages for US and China different

    /**
     * Get the AnalyticsInterface. For purposes of this app the AnalyticsInterface is very
     * simple, only logging out searches to the monitor (with slightly different messages
     * for the US and China).
     *
     * @return the AnalyticsInterface
     */
    public static AnalyticsInterface getAnalytics(){
        if (sAnalyticsInterface == null){
            switch (ConfigHelper.getValueGivenKey("analytics")){
                case "Google":
                    sAnalyticsInterface = new GaWrapper();
                    break;

                case "Insights":
                    sAnalyticsInterface = new InsightsWrapper();
                    break;

                default:
                    sAnalyticsInterface = new GaWrapper();
                    break;
            }
        }
        return sAnalyticsInterface;

    }

    /**
     * Get the in-memory cache of Places
     *
     * @return a {@link List<PlaceModel>} for the in memory cache
     */
    public static List<PlaceModel> getDataCache(){
        return sPlaceModels;
    }

    /**
     * Set the in-memory cache of Places.
     *
     * @param placeModels a {@link List<PlaceModel>} for the in memory cache
     */
    public static void setDataCache(List<PlaceModel> placeModels){
        sPlaceModels = placeModels;
    }

    /**
     * Get the LocationInterface. Just in here for demo purposes, the LocationInterface
     * here only has one implementation for US-based phones. Could set different implementations
     * based on phone specs (CDMA, gsm, etc.)
     *
     * @return the LocationInterface
     */
    public static LocationInterface getLocationManger(){
        if (sLocationInterface == null){
            sLocationInterface = new LocationManager();
        }
        return sLocationInterface;
    }

    /**
     * Function used in the App class for resetting the RealmDB when necessary.
     */
    private void resetRealm() {
        RealmConfiguration realmConfig = new RealmConfiguration
                .Builder(this)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.deleteRealm(realmConfig);
    }

    /**
     * Check the status of the network.
     *
     * @return sNetworkConnected boolean
     */
    public static boolean isNetworkConnected() {
        return sNetworkConnected;
    }

    /**
     * Set the status of the network.
     *
     * @param networkConnected sNetworkConnected boolean
     */
    public static void setNetworkConnected(boolean networkConnected) {
        sNetworkConnected = networkConnected;
    }

    /**
     * Gets the last cached search results (e.g. when searching offline).
     *
     * @return the last YelpResultsData
     */
    public static YelpResultsData getLastResults() {
        return sLastResults;
    }

    /**
     * Sets the last cached search results (e.g. for searching offline).
     *
     * @param lastResults the last YelpResultsData
     */
    public static void setLastResults(YelpResultsData lastResults) {
        sLastResults = lastResults;
    }

    /**
     * Gets the last cached search string.
     *
     * @return the last search string
     */
    public static String getLastSearched() {
        return sLastSearched;
    }

    /**
     * Sets the last cached search string..
     *
     * @param lastSearched the last search string
     */
    public static void setLastSearched(String lastSearched) {
        sLastSearched = lastSearched;
    }

    /**
     * Get mock country string.
     *
     * @return the mock country string
     */
    public static String getMockCountry(){
        return  sMockCountry;
    }




}
