package com.stagrp.mcd.ref._3_presenter;

import android.support.annotation.NonNull;

import com.stagrp.mcd.ref._4_view.interfaces.BaseView;


/**
 * A generic presenter to be implemented by all presenters in the app. Offers abstract
 * implementation of the most commonly used android fragment lifecycle methods.
 *
 * @param <View> the view which the presenter interacts with.
 */
public abstract class Presenter<View extends BaseView> {

    protected View view;

    public abstract void onStart();

    public abstract void onResume();

    public abstract void onStop();

    public abstract void onDestroy();

    public View getView() {
        return view;
    }

    public void setView(@NonNull View vx) {
        this.view = vx;
    }




}
