package com.stagrp.mcd.ref._1_data.cross_cutting.analytics;

/**
 * An implementation of {@link AnalyticsInterface} which mocks using Google Analytics. In practice
 * it just writes the dummy string to Insights.txt instead of Ga.txt.
 */

public class InsightsWrapper extends AnalyticsInterface {

    private final String LOGFILE = "Insights.txt";

    public InsightsWrapper() { super(); }
    @Override
    String getLogFileName() {
        return LOGFILE;
    }

    @Override
    public void sendMessage(String... message) {
     //TODO: write events to a log file called Insights.txt
    }
}
