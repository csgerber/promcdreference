package com.stagrp.mcd.ref._1_data.core;

import java.util.ArrayList;

import com.stagrp.mcd.ref._1_data.model.PlaceModel;
import io.realm.RealmResults;


/**
 * The RealmMapper abstracts out the logic for getting either a list of the base {@link PlaceModel}
 * objects or a single object from a {@link PlaceRealm} object or vice versa. In practice this is
 * fairly simple, but the abstraction makes it clear to other developers that a PlaceRealm is being
 * translated to PlaceModel (or the opposite).
 */
public class RealmMapper {

    /**
     * Get a list of {@link PlaceModel} from a list of {@link PlaceRealm}.
     *
     * @param places A {@link RealmResults} object containing a list of {@link PlaceRealm}
     * @return Simple ArrayList of {@link PlaceModel}
     */
    public static ArrayList<PlaceModel> getPlaceModels(RealmResults<PlaceRealm> places){

        ArrayList<PlaceModel> placeModels = new ArrayList<>();
        for (PlaceRealm place:places) {
            placeModels.add(placeRealmToPlaceModel(place));
        }

        return  placeModels;

    }
    /**
     * Instantiates a new PlaceModel from a {@link PlaceRealm}
     *
     * @param placeRealm PlaceRealm object
     */
    public static PlaceModel placeRealmToPlaceModel(PlaceRealm placeRealm) {
        PlaceModel placeModel = new PlaceModel();
        placeModel.setId(placeRealm.getId());
        placeModel.setFavorite(placeRealm.getFavorite());
        placeModel.setName(placeRealm.getName());
        placeModel.setCity(placeRealm.getCity());
        placeModel.setAddress(placeRealm.getAddress());
        placeModel.setPhone(placeRealm.getPhone());
        placeModel.setYelp(placeRealm.getYelp());
        placeModel.setImageUrl(placeRealm.getImageUrl());
        placeModel.setCategories(placeRealm.getCategories());
        placeModel.setTimestamp(placeRealm.getTimestamp());
        placeModel.setType(placeRealm.getType());
        return placeModel;
    }

    /**
     * Get a single {@link PlaceModel} from a single {@link PlaceRealm}
     *
     * @param placeRealm placeRealm object to be converted
     * @return placeModel
     */

    public static PlaceModel getPlaceModel(PlaceRealm placeRealm){
        return placeRealmToPlaceModel(placeRealm);
    }

    /**
     * Get single {@link PlaceRealm} from a single {@link PlaceModel}
     *
     * @param placeModel placeModel object to be converted
     * @return placeRealm
     */
    public static PlaceRealm getPlaceRealm(PlaceModel placeModel){
        return new PlaceRealm(placeModel);
    }


}
