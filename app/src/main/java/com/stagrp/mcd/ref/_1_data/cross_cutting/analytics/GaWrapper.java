package com.stagrp.mcd.ref._1_data.cross_cutting.analytics;


/**
 * An implementation of {@link AnalyticsInterface} which mocks using Google Analytics. In practice
 * it just writes the dummy string to Ga.txt instead of Insights.txt.
 *
 * In reality you could override AnalyticsInterface {@link AnalyticsInterface#sendMessage(String...)}
 * to customize write-outs from this analytics interface for instance.
 *
 * The real idea here is to illustrate a) use of the background thread for emptying the cache
 * (in real life these would be the only calls out to the network) and b) show the swapping
 * of analytics "implementations" (again, just writing out to a different file).
 */
public class GaWrapper extends AnalyticsInterface {

    public GaWrapper() { super(); }
    @Override
    public String getLogFileName() {
        return LOGFILE;
    }

    private final String LOGFILE = "Ga.txt";
}
