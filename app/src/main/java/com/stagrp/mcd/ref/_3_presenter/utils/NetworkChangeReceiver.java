package com.stagrp.mcd.ref._3_presenter.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.stagrp.mcd.ref.App;

/**
 * This class is an implementation of BroadcastReceiver which listens for changes in the network
 * as detected by the {@link NetworkServiceManager}. Currently it just makes a toast notification,
 * but is a demonstration of how easily custom actions could be taken whenever the network
 * is disconnected.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        if(intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {

            if (checkConnectivity(context)) {
                App.setNetworkConnected(true);
            } else {
                App.setNetworkConnected(false);
            }
        }
    }


    /**
     * Helper method for the onReceive implementation.
     *
     * @param context the context
     * @return boolean
     */
    boolean checkConnectivity(Context context) {
        NetworkServiceManager serviceManager = new NetworkServiceManager(context);
        if (serviceManager.isNetworkAvailable()) {
            return true;
        } else {
            return false;
        }
    }

}