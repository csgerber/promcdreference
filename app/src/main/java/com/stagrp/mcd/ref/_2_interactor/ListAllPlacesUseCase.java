package com.stagrp.mcd.ref._2_interactor;

import java.util.List;

import com.stagrp.mcd.ref.App;
import com.stagrp.mcd.ref._1_data.core.RepoInterface;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;

/**
 * An implementation of {@link UseCase} which is responsible for abstracting the
 * {@link RepoInterface#list()} function in addition to providing threaded operation to the user.
 *
 * Takes an optional string as input and returns a List of {@link PlaceModel}.
 */
public class ListAllPlacesUseCase extends UseCase<String, Void, List<PlaceModel>> {

   // @UiThread
    @Override
    protected List<PlaceModel> doInBackground(String... params) {
       simulateDelay(200);
       return App.getRepo().list();
    }

    public interface ListAllPlacesCallback extends UseCaseCallback<List<PlaceModel>>{}

    public ListAllPlacesUseCase(UseCaseCallback caseCallback) {
        super(caseCallback);
    }

    @Override
    protected void onPostExecute(List<PlaceModel> placeModels) {
        callback.passPostExecute( placeModels);
    }

}
