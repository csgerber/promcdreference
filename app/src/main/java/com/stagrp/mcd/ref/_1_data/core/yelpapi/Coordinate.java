package com.stagrp.mcd.ref._1_data.core.yelpapi;

import java.io.Serializable;


/**
 * Coordinates for the {@link Business}
 */
public class Coordinate implements Serializable {

    /**
     * The Latitude.
     */
    public String latitude;
    /**
     * The Longitude.
     */
    public String longitude;


    /**
     * Instantiates a new Coordinate.
     *
     * @param latitude  the latitude
     * @param longitude the longitude
     */
    public Coordinate(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }



}
