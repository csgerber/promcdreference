package com.stagrp.mcd.ref._3_presenter.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.io.Serializable;
import java.util.List;

/**
 * This is a generic adapter for the ViewHolder pattern. In this demo app it is only adapted to
 * represent places, here McDonald's restaurants. See {@link PlaceAdapter} for more details.
 *
 * @param <E>  item to be held [ie places]
 * @param <VH> viewholder parameter
 */
public abstract class GenAdapter<E extends Serializable, VH extends GenAdapter.ViewHolder>
        extends BaseAdapter {

    /**
     * The ViewHolder representing items in the adapter.
     */
    public static class ViewHolder {
        /**
         * The Item view.
         */
        public final View itemView;

        /**
         * Instantiates a new View holder.
         *
         * @param itemView the item view
         */
        public ViewHolder(View itemView) {
            this.itemView = itemView;
            this.itemView.setTag(this);
        }
    }

    private List<E> results;

    /**
     * Function to be filled out by implementation specifying any actions when the viewholder is
     * instantiated.
     *
     * @param parent   the parent viewgroup
     * @param viewType resource for view
     * @return the viewholder
     */
    public abstract VH onCreateViewHolder(ViewGroup parent, int viewType);

    /**
     * On bind view holder.
     *
     * @param holder   the ViewHolder specified above
     * @param position the position
     */
    public abstract void onBindViewHolder(VH holder, int position);

    /**
     * Action to be taken for recycling the viewholder.
     *
     * @param holder the holder
     */
    public void onViewRecycled(VH holder) {
    }

    /**
     * Sets results.
     *
     * @param results A list of results (places as implemented) held by the view (
     */
    public void setResults(List<E> results) {
        this.results = results;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return results == null ? 0 : results.size();
    }

    @Override
    public E getItem(int position) {
        return results == null ? null : results.get(position);
    }

    @Override
    public long getItemId(int position) {
        return -1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = onCreateViewHolder(parent, getItemViewType(position)).itemView;
        } else {
            onViewRecycled(getViewHolder(convertView));
        }

        onBindViewHolder(getViewHolder(convertView), position);

        return convertView;
    }

    @SuppressWarnings("unchecked")
    private VH getViewHolder(View view) {
        return (VH) view.getTag();
    }

}