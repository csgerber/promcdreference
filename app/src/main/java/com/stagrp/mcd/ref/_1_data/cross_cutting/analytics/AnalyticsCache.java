package com.stagrp.mcd.ref._1_data.cross_cutting.analytics;

import java.util.ArrayList;

/**
 * A singleton representing the cache of requests for the analytics service
 */

public class AnalyticsCache {
    private  ArrayList<String> mAnalayticsLog = new ArrayList<>();
    private static AnalyticsCache sAnalyticsCacheInstance = null;

    // empty constructor
    public AnalyticsCache() {}

    public static AnalyticsCache getAnalyticsCache() {
        if (sAnalyticsCacheInstance == null) {
            sAnalyticsCacheInstance = new AnalyticsCache();
        }
        return sAnalyticsCacheInstance;
    }

    public ArrayList<String> getAnalyticsLog() { return  mAnalayticsLog; }

    public void addAnalyticsEvent(String logString) { mAnalayticsLog.add(logString); }

    public void resetAnalyticsLog() { mAnalayticsLog.clear(); }
}
