package com.stagrp.mcd.ref._2_interactor;

/**
 * This is a callback which must be extended by each UseCase and implemented by the presenter calling
 * the UseCase. Use it in the presenter to have the view perform desired actions upon completion.
 */
public interface UseCaseCallback<Result> {
    void passPostExecute( Result result);
}
