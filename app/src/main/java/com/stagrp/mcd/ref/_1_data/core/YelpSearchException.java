package com.stagrp.mcd.ref._1_data.core;


/**
 * A simple Exception which is only thrown when bad data is returned from Yelp. Typically
 * the result of an invalid or empty search, but at a higher level than errors due to connectivity.
 */

public class YelpSearchException extends Exception {
    public YelpSearchException() {
        super();
    }
    public YelpSearchException(String detailMessage) {
        super(detailMessage);
    }
    public YelpSearchException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
    public YelpSearchException(Throwable throwable) {
        super(throwable);
    }
}
