package com.stagrp.mcd.ref._1_data.core.yelpapi;

import java.io.Serializable;
import java.util.List;


/**
 * The full Location info for a {@link Business}. The Coordinate receives its own POJO
 * should we ever want to use it for mapping purposes, but in the app only the address and city
 * are used.
 */
public class Location implements Serializable {

    /**
     * The Coordinate.
     */
    public Coordinate coordinate;
    /**
     * The Address.
     */
    public List<String> address;
    /**
     * The City.
     */
    public String city;
    /**
     * The Postal code.
     */
    public String postal_code;


}
