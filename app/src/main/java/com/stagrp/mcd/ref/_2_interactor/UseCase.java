package com.stagrp.mcd.ref._2_interactor;

import android.os.AsyncTask;

/**
 * Generic implementation of a UseCase. Each UseCase is an implementation of a generic android
 * Asynctask so work can be done in the background and only have the view update upon completion.
 *
 * UseCases should only be called in the presentation layer, and offer an abstract interface
 * to the core business logic. Most UseCases will call a repository to perform the desired action.
 * Note that all of the CRUD operations in {@link com.stagrp.mcd.ref._1_data.core.RepoInterface}
 * have an associated UseCase.
 *
 * Each presenter must implement a {@link UseCaseCallback} for each UseCase created. This
 * allows different presenters to perform different actions upon completion as desired.
 *
 * @param <Params>   input to the UseCase
 * @param <Progress> not used in any UseCases
 * @param <Result>   result outputted by a UseCase
 */
public abstract class UseCase<Params, Progress, Result> extends AsyncTask<Params, Progress, Result>  {


    protected UseCaseCallback<Result> callback;

    /**
     * Instantiates a new UseCaseCallback. The appropriate UseCaseCallBack <i>must</i> be implemented
     * by a presenter calling a UseCase.
     *
     * @param caseCallback the case callback
     */
    public UseCase(UseCaseCallback<Result> caseCallback) {
        if (caseCallback != null)
            callback = caseCallback;
    }

    /***
     * Simulates a delay in processing. This is used so that one can see how the progress bar works
     * in the views.
     *
     * @param nDelay the delay in milliseconds
     */
    protected void simulateDelay(int nDelay){
        try {
            Thread.sleep(nDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
