package com.stagrp.mcd.ref._1_data.model;

import java.io.Serializable;
import java.util.UUID;

/**
 * The base model for a Place (in this demo app always a McDonald's restaurant). This is only
 * object which should be referred to be higher-level components.
 */
public class PlaceModel implements Serializable {

    private String id;
    private int favorite;
    private String name;
    private String city;
    private String address;
    private String phone;
    private String yelp;
    private String imageUrl;
    private String categories;
    private String type;
    private long timestamp;


    /**
     * Instantiates a new PlaceModel with a null constructor. Useful for creating dummies or
     * testing.
     */
    public PlaceModel() {
    }


    /**
     * Instantiates a new Place model based on data provided. The id is generated automatically in
     * this constructor.
     *
     * @param favorite   0 = not favorited, 1 = favorited
     * @param name       the name
     * @param city       the city
     * @param address    the address
     * @param phone      the phone
     * @param yelp       the URL of the yelp page
     * @param imageUrl   the image url
     * @param categories the categories
     * @param timestamp  the timestamp (epoch milliseconds)
     * @param type       the type
     */
    public PlaceModel(int favorite, String name, String city, String address, String phone, String yelp,
                      String imageUrl, String categories, long timestamp, String type) {

        this.favorite = favorite;
        this.name = name;
        this.city = city;
        this.address = address;
        this.phone = phone;
        this.yelp = yelp;
        this.imageUrl = imageUrl;
        if(this.imageUrl.equals("")) {
            imageUrl = "https://cdn2.hubspot.net/hub/1547213/file-3362151540-jpg/blog-files/questionmark.jpg";
        }
        this.categories = categories;
        this.timestamp = timestamp;
        this.type= type;

        //use guid here.
        int result = super.hashCode();
        result = 31 * result + favorite;
        if (name != null)
            result = 31 * result + name.hashCode();
        if (city != null)
            result = 31 * result + city.hashCode();
        if (address != null)
            result = 31 * result + address.hashCode();
        if (phone != null)
            result = 31 * result + phone.hashCode();
        if (yelp != null)
            result = 31 * result + yelp.hashCode();
        if (imageUrl != null)
            result = 31 * result + imageUrl.hashCode();

        id = String.valueOf(new UUID((long)result, (long)-result).randomUUID());

    }


    /**
     * Gets id (UUID string)
     *
     * @return the UUID string
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id. Should be in UUID format.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets categories. In the demo app "categories" just defaults to Restaurant.
     *
     * @return category string
     */
    public String getCategories() {
        return categories;
    }

    /**
     * Sets category.
     *
     * @param categories category string (e.g. "restaurants")
     */
    public void setCategories(String categories) {
        this.categories = categories;
    }

    /**
     * Gets timestamp  -- epoch milliseconds
     *
     * @return the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the timestamp.
     *
     * @param timestamp the timestamp
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Gets favorite status (0 = nonfavorite, 1 = favorite).
     *
     * @return integer of favorite status
     */
    public int getFavorite() {
        return favorite;
    }

    /**
     * Sets favorite status.
     *
     * @param favorite integer of favorite status (0 or 1)
     */
    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    /**
     * Gets name (in the demo app always "McDonald's")
     *
     * @return name string
     */
    public String getName() {
        return name;
    }

    /**
     * Set the place name.
     *
     * @param name name string
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets city or ZIP location (string).
     *
     * @return location string.
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets city or ZIP location.
     *
     * @param city location string.
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets address.
     *
     * @return address string
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address address string
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets phone number of the place.
     *
     * @return phone number string
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets phone number.
     *
     * @param phone phone number string.
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Gets yelp URL for place.
     *
     * @return text yelp URL
     */
    public String getYelp() {
        return yelp;
    }

    /**
     * Sets yelp URL.
     *
     * @param yelp yelp URL string.
     */
    public void setYelp(String yelp) {
        this.yelp = yelp;
    }

    /**
     * Gets image url.
     *
     * @return the image url
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets image url.
     *
     * @param imageUrl the image url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(String type) {
        this.type = type;
    }
}
