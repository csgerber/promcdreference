package com.stagrp.mcd.ref._3_presenter;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.telephony.PhoneNumberUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.stagrp.mcd.ref.App;
import com.stagrp.mcd.ref.R;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;
import com.stagrp.mcd.ref._2_interactor.DeletePlaceUseCase;
import com.stagrp.mcd.ref._2_interactor.ListAllPlacesUseCase;
import com.stagrp.mcd.ref._2_interactor.UpdatePlaceUseCase;
import com.stagrp.mcd.ref._3_presenter.adapters.PlaceAdapter;
import com.stagrp.mcd.ref._4_view.interfaces.PlaceView;

import java.util.ArrayList;
import java.util.List;


/**
 * The PlacePresenter is the presenter for the fragment displaying all the places
 * in a list or grid (depending on configuration). It implements several callbacks
 * as the PlaceFragment houses more operations than other fragments, but the passPostExecute
 * is always to refresh the list of places.
 */
public class PlacePresenter extends Presenter<PlaceView> implements
        ListAllPlacesUseCase.ListAllPlacesCallback,
        DeletePlaceUseCase.DeletePlaceCallback,
        UpdatePlaceUseCase.UpdatePlaceCallback

{

    /**
     * In a "broader" version of the app different types were used (e.g. restaurants, bars, etc.)
     * but the demo reference app is only limited to McDonald's restaurants.
     */
    public static final String sRESTAURANTS = "restaurants";

    private PlaceAdapter mAdapter;

    private boolean toggleFav = false;

    /**
     * The onStart here has to instantiate the list of actions for when places are clicked
     * as well as setup the ArrayAdapter for displaying the places, and as such contains
     * more logic than onStarts for other presenters.
     */
    @Override
    public void onStart() {

        App.getAnalytics().initExecutor();

        mAdapter = new PlaceAdapter(view.provideContext(), new PlaceAdapter.OnItemClickListener() {
            public void onItemClick(final PlaceModel placeClicked) {


                AlertDialog.Builder builder = new AlertDialog.Builder(view.provideContext());

                ListView modeList = new ListView(view.provideContext());

                String[] stringArray = new String[]{
                        "Edit ",  //0
                        "Share ", //1
                        "Map of ", //2
                        "Dial ",  //3
                        "Yelp site ",  //4
                        "Navigate to ",  //5
                        "Delete ", //6
                        "Cancel ",//7
                        placeClicked.getFavorite() == 1 ? "Unfavorite this Place" : "Favorite this Place" //8

                };

                ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(view.provideContext(), android.R.layout.simple_list_item_1, android.R.id.text1, stringArray);
                modeList.setAdapter(modeAdapter);
                builder.setView(modeList);
                final Dialog dialog = builder.create();


                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                dialog.setTitle(placeClicked.getName());
                dialog.show();
                modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                        try {
                            switch (position) {
                                case 0:
                                    //edit
                                    //does nothing yet, will need to be implemented.
                                    editPlaceDialog(placeClicked);

                                    break;
                                case 1:
                                    //share
                                    sharePlace(placeClicked);
                                    break;

                                case 2:
                                    //map of
                                    mapPlace(placeClicked);
                                    break;
                                case 3:
                                    //dial
                                    dialPlace(placeClicked);
                                    break;
                                case 4:
                                    //yelp site
                                    yelpSite(placeClicked);
                                    break;
                                case 5:
                                    //navigate to : needs to be implemented
                                    navigateTo(placeClicked);
                                    break;
                                //delete
                                case 6:
                                    deletePlace(placeClicked);
                                    break;
                                case 7:
                                    //cancel
                                    break;

                                //toggle the favorite
                                case 8:
                                    toggleFavoriteOfPlace(placeClicked);

                                    break;
                            }
                        } catch (Exception e) {
                            //gracefully handle exceptions
                            e.printStackTrace();
                            showError(e.getMessage());

                        }

                        dialog.dismiss();
                    }
                });

            }
        });

    }

    @Override
    public void onDestroy() {
        App.getAnalytics().shutdown();
    }

    /**
     * Note that any time the fragment is resumed we execute a listAllPlacesUseCase to make
     * sure the places are up to date.
     */
    @Override
    public void onResume() {
        view.startProgress();


        ListAllPlacesUseCase listAllPlacesUseCase = new ListAllPlacesUseCase(this);
        listAllPlacesUseCase.execute("");

    }

    @Override
    public void onStop() {
    }


    /**
     * Edit place dialog. Will be extracted out to a fragment.
     *
     * @param place the place being edited.
     */
//TODO this is a fragment.
    public void editPlaceDialog(final PlaceModel place) {
        final Dialog dialog = new Dialog(view.getRouter());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.frag_scroll_layout_edit);

        final EditText editName = (EditText) dialog.findViewById(R.id.edit_main_name);
        final EditText editCity = (EditText) dialog.findViewById(R.id.edit_main_city);
        final EditText editAddress = (EditText) dialog.findViewById(R.id.edit_address);
        final EditText editPhone = (EditText) dialog.findViewById(R.id.edit_phone);
        Button submit = (Button) dialog.findViewById(R.id.edit_button_save);
        Button cancel = (Button) dialog.findViewById(R.id.edit_button_cancel);
        final CheckBox checkFav = (CheckBox) dialog.findViewById(R.id.edit_check_favorite);
        final Spinner editType = (Spinner) dialog.findViewById(R.id.edit_category_spinner);
        ArrayAdapter<CharSequence> spinadapt = ArrayAdapter.createFromResource(view.provideContext(),
                R.array.category_options, android.R.layout.simple_spinner_item);
        spinadapt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        editType.setAdapter(spinadapt);
        // set the spinner to our current choice
        int spinpos = spinadapt.getPosition(place.getType());
        editType.setSelection(spinpos);

        editName.setText(place.getName());
        editCity.setText(place.getCity());
        editAddress.setText(place.getAddress());
        editPhone.setText(place.getPhone());

        checkFav.setChecked(place.getFavorite() == 1);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editName.getText().toString();
                String city = editCity.getText().toString();
                String address = editAddress.getText().toString();
                String yelp = place.getYelp();
                String phone = editPhone.getText().toString();
                String type = editType.getSelectedItem().toString();
                int fav = checkFav.isChecked() ? 1 : 0;
                updatePlace(place, name, city, address, phone, yelp, fav, type);

                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    /**
     * Share place logic. Requires extracting the place info and firing off an intent holding info
     * wherever the user specifies
     *
     * @param placeClicked the place clicked
     */
    public void sharePlace(PlaceModel placeClicked) {

        String strSubject = "Check out: " + placeClicked.getName();
        String strMessage = "\n\n"; //give the user some room to type a message
        strMessage += "Restaurant: " + placeClicked.getName();
        strMessage += "\nAddress: " + placeClicked.getAddress() + ", " + placeClicked.getCity();
        strMessage += " \n\nPhone: " + PhoneNumberUtils.formatNumber(placeClicked.getPhone());
        strMessage += " \nYelp page: " + placeClicked.getYelp();
        if (placeClicked.getFavorite() == 1) {
            strMessage += "\n[This is one of my favorite restaurants]";
        }
        strMessage += "\n\nsent from ProPlaces";

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, strSubject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, strMessage);
        view.provideContext().startActivity(Intent.createChooser(sharingIntent, "Choose client"));
    }

    /**
     * Fire off an intent launching the location of the place in the user's default map.
     *
     * @param place the place clicked
     */
    public void mapPlace(PlaceModel place) {
        Intent intentMapOf = new Intent(Intent.ACTION_VIEW,
                Uri.parse("geo:0,0?q=" + place.getAddress() + " " + place.getCity()));
        view.provideContext().startActivity(intentMapOf);
    }

    /**
     * Dial the phone number listed for the restaurant.
     *
     * @param place the place clicked
     */
    public void dialPlace(PlaceModel place) {
        Intent intentDial = new Intent(Intent.ACTION_CALL);
        intentDial.setData(Uri.parse("tel:" + place.getPhone()));
        //check for permissions
        if (ActivityCompat.checkSelfPermission(view.provideContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            view.provideContext().startActivity(intentDial);
        }
    }

    /**
     * Pull up the yelp site for the restaurant.
     *
     * @param place the place clicked
     */
    public void yelpSite(PlaceModel place) {
        Intent intentYelp = new Intent(Intent.ACTION_VIEW);
        intentYelp.setData(Uri.parse(place.getYelp()));
        view.provideContext().startActivity(intentYelp);
    }

    /**
     * Navigate to the place using google maps (slightly different than just pulling up a generic
     * map with geolocation).
     *
     * @param place the place
     */
    public void navigateTo(PlaceModel place) {
        String map = "http://maps.google.co.in/maps?q=" + place.getAddress() + ", " + place.getCity();
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(map));
        view.provideContext().startActivity(intent);
    }

    /**
     * Delete the place. Fires off a deleteplaceusecase.
     *
     * @param place the place to be deleted
     */
    public void deletePlace(PlaceModel place) {


        DeletePlaceUseCase deletePlaceUseCase = new DeletePlaceUseCase(this);
        deletePlaceUseCase.execute(place);

    }

    /**
     * Toggle favorite status of place. Fires off an updateplaceusecase as this requires operating
     * on the DB.
     *
     * @param place the place
     */
    public void toggleFavoriteOfPlace(PlaceModel place) {

        int favoriteStatus = place.getFavorite() == 0 ? 1 : 0;
        place.setFavorite(favoriteStatus);
        toggleFav = false;
        UpdatePlaceUseCase updatePlaceUseCase = new UpdatePlaceUseCase(this);
        updatePlaceUseCase.execute(place);


    }

    /**
     * Show error message for the dialog when a place is clicked.
     *
     * @param strErrorMessage the error message to be displayed.
     */
    public void showError(String strErrorMessage){

        final AlertDialog.Builder builder = new AlertDialog.Builder(view.provideContext());
        builder.setTitle("Error")
                .setMessage(strErrorMessage)
                .setCancelable(false);

        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    /**
     * List favorites. Note there are no callbacks used here as this only filters based on
     * the in-memory cache of places (which also speeds up interaction).
     */
    public void listFavs(){

        toggleFav = !toggleFav;
        List<PlaceModel> placeModels = App.getDataCache();

        if (placeModels != null){
            if (toggleFav) {
                List<PlaceModel> accumulator = new ArrayList<>();
                for (PlaceModel place : placeModels) {
                    if (place.getFavorite() == 1){
                        accumulator.add(place);
                    }
                }
                view.displayPlaces(mAdapter,accumulator);
            } else {
                view.displayPlaces(mAdapter,placeModels);
            }
        }



    }


    /**
     * Update place information. Fires off an updateplaceusecase.
     *
     * @param place    the place [object]
     * @param name     the name [string]
     * @param city     the city [string]
     * @param address  the address [string]
     * @param phone    the phone [string]
     * @param yelp     the yelp [string]
     * @param favorite the favorite status [int, 1=favorite, 0=not favorite]
     * @param type     the type [only ever "restaurants" here]
     */
    public void updatePlace(PlaceModel place, String name, String city, String address,
                            String phone, String yelp, int favorite, String type){

        place.setName(name);
        place.setCity(city);
        place.setAddress(address);
        place.setPhone(phone);
        place.setYelp(yelp);
        place.setFavorite(favorite);
        place.setType(type);

        UpdatePlaceUseCase updatePlaceUseCase = new UpdatePlaceUseCase(this);
        updatePlaceUseCase.execute(place);

    }

    /**
     * The usecasecallback for the PlacePresenter. No matter what operation is carried out
     * we always want to refresh this presenter with the new list of places.
     * @param placeModels a list of {@link PlaceModel}
     */
    @Override
    public void passPostExecute(List<PlaceModel> placeModels) {
        App.setDataCache(placeModels);
        view.displayPlaces(mAdapter,placeModels);
        view.stopProgress();
    }

}
