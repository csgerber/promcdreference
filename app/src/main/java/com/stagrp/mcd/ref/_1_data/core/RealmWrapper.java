package com.stagrp.mcd.ref._1_data.core;

import com.stagrp.mcd.ref.App;
import com.stagrp.mcd.ref._1_data.model.PlaceModel;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


/**
 * This class is responsible for implementing the abstract logic in {@link RepoInterface} necessary
 * to use Realm as a backend database. Implements the CRUD operations in RepoInterface with
 * Realm-specific instructions. All methods here are overrides of {@link RepoInterface}, so
 * see that file for description of their operation.
 */

public class RealmWrapper implements RepoInterface {

    @Override
    public void create(PlaceModel place) {

        Realm realm = Realm.getInstance(App.getContext());
        realm.beginTransaction();
        realm.copyToRealm(RealmMapper.getPlaceRealm(place));
        realm.commitTransaction();
    }

    @Override
    public PlaceModel read(String id) {
        Realm realm = Realm.getInstance(App.getContext());
        PlaceRealm place = realm.where(PlaceRealm.class).equalTo("id", id).findFirst();
       return RealmMapper.getPlaceModel(place);
    }

    @Override
    public void update(PlaceModel placeModel) {

        Realm realm = Realm.getInstance(App.getContext());
        realm.beginTransaction();

//        PlaceRealm placeRealm = realm.where(PlaceRealm.class).equalTo("id", placeModel.getId()).findFirst();

        realm.copyToRealmOrUpdate(RealmMapper.getPlaceRealm(placeModel));
        realm.commitTransaction();

    }

    @Override
    public void delete(String id) {
        Realm realm = Realm.getInstance(App.getContext());
        realm.beginTransaction();
        PlaceRealm place = realm.where(PlaceRealm.class).equalTo("id", id).findFirst();
        place.removeFromRealm();
        realm.commitTransaction();


    }


    @Override
    public List<PlaceModel> list() {
        Realm realm = Realm.getInstance(App.getContext());
        RealmResults<PlaceRealm> places = realm.allObjects(PlaceRealm.class).where()

                .findAllSorted("timestamp", Sort.DESCENDING);
        return  RealmMapper.getPlaceModels(places);

    }

}
