package com.stagrp.mcd.ref._1_data.core;

import com.stagrp.mcd.ref._1_data.core.yelpapi.YelpResultsData;


/**
 * Interface representing abstraction of getting Restaurant data from yelp. Only this method
 * should be called from the UseCases requesting Yelp data.
 */

public interface RestaurantDirectoryInterface {

    /**
     * Fetch yelp results data as a {@link YelpResultsData} object.
     *
     * @param name     Restaurant name (note: always McDonald's here)
     * @param location Restaurant location (city or ZIP)
     * @return {@link YelpResultsData} object
     * @throws YelpSearchException Throw a {@link YelpSearchException} if an error occurs in retrieval
     */
    YelpResultsData fetchData(String name, String location) throws YelpSearchException;
}
